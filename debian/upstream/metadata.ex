# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/bottles/issues
# Bug-Submit: https://github.com/<user>/bottles/issues/new
# Changelog: https://github.com/<user>/bottles/blob/master/CHANGES
# Documentation: https://github.com/<user>/bottles/wiki
# Repository-Browse: https://github.com/<user>/bottles
# Repository: https://github.com/<user>/bottles.git
